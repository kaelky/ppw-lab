from django.conf.urls import url
from .views import index
from .views import testing

#url for app, add your URL Configuration
urlpatterns = [
    url(r'^$', index, name = 'index'),
    url(r'^testing/', testing, name = 'testing'),
]
