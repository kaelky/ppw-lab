from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .api_enterkomputer import get_drones, get_soundcard, get_optical

response = {'author' : 'Kaelky'}

#Fungsi ini menentukan tampilan layar utama yang akan ditampilkan apakah tampilan yang sudah login atau belum
def index(request):
    print ("#==> masuk index")
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('lab-9:profile'))
    else:
        html = 'lab_9/session/login.html'
        return render(request, html, response)

#mengatur seluruh data ke dalam session
def set_data_for_session(res, request):
    response['username'] = request.session['user_login']
    response['access_token'] = request.session['access_token']
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']
    response['drones'] = get_drones().json()

    if 'drones' in request.session.keys():
        response['fav_drones'] = request.session['drones']
    else:
        response['fav_drones'] = []

def set_data_for_session_soundcard(res, request):
    response['username'] = request.session['user_login']
    response['access_token'] = request.session['access_token']
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']
    response['soundcards'] = get_soundcard().json()

    if 'soundcards' in request.session.keys():
        response['fav_soundcards'] = request.session['soundcards']
    else:
        response['fav_soundcards'] = []

def set_data_for_session_optical(res, request):
    response['username'] = request.session['user_login']
    response['access_token'] = request.session['access_token']
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']
    response['opticals'] = get_optical().json()

    if 'opticals' in request.session.keys():
        response['fav_opticals'] = request.session['opticals']
    else:
        response['fav_opticals'] = []

#Menampilkan profile dengan status login session jika user telah melakukan session login
def profile(request):
    print ("#==> profile")
    if 'user_login' not in request.session.keys():
        return HttpResponseRedirect(reverse('lab-9:index'))
    set_data_for_session(response, request)
    set_data_for_session_soundcard(response, request)
    set_data_for_session_optical(response, request)
    html = 'lab_9/session/profile.html'
    return render(request, html, response)

#Menambahkan favorite drones ke session
def add_session_drones(request, id):
    ssn_key = request.session.keys()
    if not 'drones' in ssn_key:
        print ("# init drones ")
        request.session['drones'] = [id]
    else:
        drones = request.session['drones']
        print ("# existing drones => ", drones)
        if id not in drones:
            print ('# add new item, then save to session')
            drones.append(id)
            request.session['drones'] = drones

    messages.success(request, "Berhasil tambah drone favorite")
    return HttpResponseRedirect(reverse('lab-9:profile'))

def add_session_soundcards(request, id):
    ssn_key = request.session.keys()
    if not 'soundcards' in ssn_key:
        print ("# init soundcards ")
        request.session['soundcards'] = [id]
    else:
        soundcards = request.session['soundcards']
        print ("# existing soundcards => ", soundcards)
        if id not in soundcards:
            print ('# add new item, then save to session')
            soundcards.append(id)
            request.session['soundcards'] = soundcards

    messages.success(request, "Berhasil tambah soundcard favorite")
    return HttpResponseRedirect(reverse('lab-9:profile'))

def add_session_opticals(request, id):
    ssn_key = request.session.keys()
    if not 'opticals' in ssn_key:
        print ("# init opticals ")
        request.session['opticals'] = [id]
    else:
        opticals = request.session['opticals']
        print ("# existing opticals => ", opticals)
        if id not in opticals:
            print ('# add new item, then save to session')
            opticals.append(id)
            request.session['opticals'] = opticals

    messages.success(request, "Berhasil tambah optical favorite")
    return HttpResponseRedirect(reverse('lab-9:profile'))

#Menghapus favorite drones dari session
def del_session_drones(request, id):
    print ("# DEL drones")
    drones = request.session['drones']
    print ("before = ", drones)
    drones.remove(id) #untuk remove id tertentu dari list
    request.session['drones'] = drones
    print ("after = ", drones)

    messages.error(request, "Berhasil hapus dari favorite")
    return HttpResponseRedirect(reverse('lab-9:profile'))

def del_session_soundcards(request, id):
    print ("# DEL soundcards")
    soundcards = request.session['soundcards']
    print ("before = ", soundcards)
    soundcards.remove(id) #untuk remove id tertentu dari list
    request.session['soundcards'] = soundcards
    print ("after = ", soundcards)

    messages.error(request, "Berhasil hapus soundcard dari favorite")
    return HttpResponseRedirect(reverse('lab-9:profile'))

def del_session_opticals(request, id):
    print ("# DEL opticals")
    opticals = request.session['opticals']
    print ("before = ", opticals)
    opticals.remove(id) #untuk remove id tertentu dari list
    request.session['opticals'] = opticals
    print ("after = ", opticals)

    messages.error(request, "Berhasil hapus optical dari favorite")
    return HttpResponseRedirect(reverse('lab-9:profile'))

#Menghapus seluruh drones pada session
def clear_session_drones(request):
    print ("# CLEAR session drones")
    print ("before 1 = ", request.session['drones'])
    del request.session['drones']

    messages.error(request, "Berhasil reset favorite drones")
    return HttpResponseRedirect(reverse('lab-9:profile'))

def clear_session_soundcards(request):
    print ("# CLEAR session soundcards")
    print ("before 1 = ", request.session['soundcards'])
    del request.session['soundcards']

    messages.error(request, "Berhasil reset favorite soundcards")
    return HttpResponseRedirect(reverse('lab-9:profile'))

def clear_session_opticals(request):
    print ("# CLEAR session opticals")
    print ("before 1 = ", request.session['opticals'])
    del request.session['opticals']

    messages.error(request, "Berhasil reset favorite opticals")
    return HttpResponseRedirect(reverse('lab-9:profile'))

#Mengetes apakah login cookies telah dilakukan, jika belum maka akan diminta untuk login cookies.
def cookie_login(request):
    print ("#==> masuk login dengan cookies")
    if is_login(request):
        return HttpResponseRedirect(reverse('lab-9:cookie_profile'))
    else:
        html = 'lab_9/cookie/login.html'
        return render(request, html, response)

#Autentikasi apakah password dan username yang dimasukkan untuk login cookie apakah benar
#Jika benar, pemanggilan login with cookies akan dilakukan pada fungsi cookie_login
def cookie_auth_login(request):
    print ("# Auth login")
    if request.method == "POST":
        user_login = request.POST['username']
        user_password = request.POST['password']

        if my_cookie_auth(user_login, user_password):
            print ("#SET cookies")
            res = HttpResponseRedirect(reverse('lab-9:cookie_login'))

            res.set_cookie('user_login', user_login)
            res.set_cookie('user_password', user_password)

            return res
        else:
            msg = "Username atau Password Salah"
            messages.error(request, msg)
            return HttpResponseRedirect(reverse('lab-9:cookie_login'))
    else:
        return HttpResponseRedirect(reverse('lab-9:cookie_login'))


#Fungsi yang akan merender ke profile cookies jika password dan username yang dilogin pada cookies sesuai dengan yang diinginkan
def cookie_profile(request):
    print ("# cookie profile ")
    if not is_login(request):
        print ("belum login")
        return HttpResponseRedirect(reverse('lab-9:cookie_login'))
    else:
        in_uname = request.COOKIES['user_login']
        in_pwd= request.COOKIES['user_password']
        # jika cookie diset secara manual (usaha hacking), distop dengan cara berikut
        # agar bisa masuk kembali, maka hapus secara manual cookies yang sudah diset
        if my_cookie_auth(in_uname, in_pwd):
            html = "lab_9/cookie/profile.html"
            res =  render(request, html, response)
            return res
        else:
            print ("#login dulu")
            msg = "Kamu tidak memiliki akses"
            messages.error(request, msg)
            html = "lab_9/cookie/login.html"
            return render(request, html, response)

#Menghapus password dan username dari cookies, lalu mengarahkan ke halaman login cookies
def cookie_clear(request):
    res = HttpResponseRedirect('/lab-9/cookie/login')
    res.delete_cookie('user_password')
    res.delete_cookie('user_login')

    msg = "Anda berhasil logout. Cookies direset"
    messages.info(request, msg)
    return res

#Menguji apakah in_uname dan in_pwd sama dengan username dan password yang diharapkan
def my_cookie_auth(in_uname, in_pwd):
    my_uname = "kaelky"
    my_pwd = "1606878013"
    return in_uname == my_uname and in_pwd == my_pwd

#Mengecek apakah username dan password ada di cookies
def is_login(request):
    return 'user_login' in request.COOKIES and 'user_password' in request.COOKIES
