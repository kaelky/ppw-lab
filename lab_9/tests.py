from django.test import TestCase, Client
from django.urls import resolve
from .api_enterkomputer import get_drones, get_soundcard, get_optical
from .csui_helper import get_access_token, verify_user, get_data_user
from .custom_auth import auth_login, auth_logout
import environ

root = environ.Path(__file__)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

class Lab9UnitTest(TestCase):
    def test_lab_9_url_is_exist(self):
        response = Client().get('/lab-9/')
        self.assertEqual(response.status_code, 200)

    def lab_9_using_index_func(self):
        found = resolve('/lab-9/')
        self.assertEqual(found.func, index)

    def test_lab_9_not_logged_in (self):
        response = self.client.get('/lab-9/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('lab_9/session/login.html')

    def test_lab_9_logged_in (self):
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        response = self.client.post('/lab-9/custom_auth/login/', {'username' : username, 'password' : password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/')
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('lab_9/session/profile.html')


    def test_profile_not_logged_in(self):
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 302)

    def test_profile_logged_in(self):
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        response = self.client.post('/lab-9/custom_auth/login/', {'username': username, 'password': password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 200)

    def test_add_delete_and_reset_favorite_drones(self):
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        response = self.client.post('/lab-9/custom_auth/login/', {'username': username, 'password': password})
        self.assertEqual(response.status_code, 302)

        #add drone
        response = self.client.post('/lab-9/add_session_drones/'+get_drones().json()[0]["id"]+'/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil tambah drone favorite", html_response)

        response = self.client.post('/lab-9/add_session_drones/'+get_drones().json()[1]["id"]+'/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertIn("Berhasil tambah drone favorite", html_response)

        #delete drone
        response = self.client.post('/lab-9/del_session_drones/'+get_drones().json()[0]["id"]+'/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil hapus dari favorite", html_response)

        #reset drones
        response = self.client.post('/lab-9/clear_session_drones/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil reset favorite drones", html_response)

    def test_add_delete_and_reset_favorite_soundcard(self):
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        response = self.client.post('/lab-9/custom_auth/login/', {'username': username, 'password': password})
        self.assertEqual(response.status_code, 302)

        #add soundcard
        response = self.client.post('/lab-9/add_session_soundcards/'+get_soundcard().json()[0]["id"]+'/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil tambah soundcard favorite", html_response)

        response = self.client.post('/lab-9/add_session_soundcards/'+get_soundcard().json()[1]["id"]+'/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertIn("Berhasil tambah soundcard favorite", html_response)

        #delete soundcard
        response = self.client.post('/lab-9/del_session_soundcards/'+get_soundcard().json()[0]["id"]+'/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil hapus soundcard dari favorite", html_response)

        #reset soundcard
        response = self.client.post('/lab-9/clear_session_soundcards/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil reset favorite soundcards", html_response)

    def test_add_delete_and_reset_favorite_soundcard(self):
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        response = self.client.post('/lab-9/custom_auth/login/', {'username': username, 'password': password})
        self.assertEqual(response.status_code, 302)

        #add soundcard
        response = self.client.post('/lab-9/add_session_opticals/'+get_optical().json()[0]["id"]+'/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil tambah optical favorite", html_response)

        response = self.client.post('/lab-9/add_session_opticals/'+get_optical().json()[1]["id"]+'/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertIn("Berhasil tambah optical favorite", html_response)

        #delete soundcard
        response = self.client.post('/lab-9/del_session_opticals/'+get_optical().json()[0]["id"]+'/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil hapus optical dari favorite", html_response)

        #reset soundcard
        response = self.client.post('/lab-9/clear_session_opticals/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil reset favorite opticals", html_response)

    def test_logout(self):
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        response = self.client.post('/lab-9/custom_auth/login/', {'username': username, 'password': password})
        self.assertEqual(response.status_code, 302)
        response = self.client.post('/lab-9/custom_auth/logout/')
        html_response = self.client.get('/lab-9/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Anda berhasil logout. Semua session Anda sudah dihapus", html_response)

    # =================================================================================================================== #

    # COOKIES

    def test_cookie(self):
        #not logged in
        response = self.client.get('/lab-9/cookie/login/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(response.status_code, 302)

        #login using HTTP GET method
        response = self.client.get('/lab-9/cookie/auth_login/')
        self.assertEqual(response.status_code, 302)

        #login failed, invalid pass and uname
        response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'username', 'password': 'password'})
        html_response = self.client.get('/lab-9/cookie/login/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Username atau Password Salah", html_response)

        #try to set manual cookies
        self.client.cookies.load({"user_login": "u", "user_password": "p"})
        response = self.client.get('/lab-9/cookie/profile/')
        html_response = response.content.decode('utf-8')
        self.assertIn("Kamu tidak memiliki akses", html_response)

        #login successed
        self.client = Client()
        response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'kaelky', 'password': '1606878013'})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/cookie/login/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(response.status_code, 200)

        #logout
        response = self.client.post('/lab-9/cookie/clear/')
        html_response = self.client.get('/lab-9/cookie/profile/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Anda berhasil logout. Cookies direset", html_response)

    # =================================================================================================================== #


    def test_custom_auth_invalid_login(self):
        username = 'windi'
        password = 'chandra'
        response = Client().post('/lab-9/custom_auth/login/', {'username': username, 'password':password})
        self.assertEqual(response.status_code, 302)

    def test_custom_auth_login_and_logout(self):
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        response = Client().post('/lab-9/custom_auth/login/', {'username': username, 'password':password})
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('lab_9/session/profile.html')
        response = Client().get('/lab-9/custom_auth/logout/')
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('lab_9/session/login.html')

    def test_verify_user(self):
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        access_token = get_access_token(username, password)
        verified_user = verify_user(access_token)
        self.assertTrue(verified_user is not None)

    def test_get_data_user(self):
        access_token = get_access_token(env("SSO_USERNAME"), env("SSO_PASSWORD"))
        verified_user = verify_user(access_token)
        data_user = get_data_user(access_token, verified_user["identity_number"])
