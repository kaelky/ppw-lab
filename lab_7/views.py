from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import os
import json

response = {}
response['author'] = 'Kaelky'
csui_helper = CSUIhelper()

def index(request):
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    friend_list = Friend.objects.all()
    page = request.GET.get("page", 1)
    #membuat sebuah objek Paginator dengan per_page 13 dan mengambil data dari get_mahasiswa_list
    paginator = Paginator(mahasiswa_list, 13)
    mahasiswa_list = paginateView(paginator, page)
    html = 'lab_7/lab_7.html'
    response['mahasiswa_list'] = mahasiswa_list
    response['friend_list'] = friend_list
    return render (request, html, response)

def paginateView (paginator , page) :
    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage :
        users = paginator.page(paginator.num_pages)
    return users

def friend_list (request) :
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render (request, html, response)

def friend_list_json(request) :
    friends = [obj.as_dict() for obj in Friend.objects.all()]
    return JsonResponse ({"results" : friends}, content_type = 'application/json')

@csrf_exempt
def add_friend (request) :
    if request.method == 'POST' :
        name = request.POST['name']
        npm = request.POST['npm']
        daftar_teman = Friend.objects.all()
        is_already = False
        for teman in daftar_teman:
            if (npm == teman.npm and name == teman.friend_name):
                is_already = True
        if is_already:
            return JsonResponse(None)
        else :
            friend = Friend(friend_name=name , npm=npm)
            friend.save()
            return JsonResponse (friend.as_dict())

@csrf_exempt
def delete_friend (request) :
    if request.method == 'POST':
        npm = request.POST['npm']
        Friend.objects.filter (npm=npm).delete()
        return HttpResponseRedirect('/lab-7/')

@csrf_exempt
def validate_npm (request):
    nama = request.POST.get('name')
    npm = request.POST.get('npm')
    data = {
        'is_taken' : Friend.objects.filter (npm=npm).exists() and Friend.objects.filter (friend_name=nama).exists()
        }
    return JsonResponse(data)
