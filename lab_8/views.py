from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse

response = {}
response['author'] = 'Kaelky'

def index(request) :
    html = 'lab_8/lab_8.html'
    return render(request, html, response)
