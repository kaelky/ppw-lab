//FB initiation function
window.fbAsyncInit = function() {
  FB.init({
    appId      : '136338037076722',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });

  //Fungsi yang melakukan cek status login (getLoginStatus)
  FB.getLoginStatus(function(response){
    if (response.status === 'connected'){
      render(true)
    }
    else if (response.status === 'not_authorized') {
      render(false)
    }
    else {
      render(false)
    }
  });
};

(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "https://connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));

 var render = function(loginFlag){
   if (loginFlag) {
     getUserData(user => {
       $('#lab8').html(
         //Tampilan HTML
         '<div class="profile container-fluid bg-overlay">' +
            '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
            '<div class="data">' +
              '<h1>' + '<pre>' + 'Name      : ' + user.name + '</pre>' + '</h1>' +
              '<h2>' + '<pre>' + 'About me  : ' + user.about + '</pre>' + '</h2>' +
              '<h3>' + '<pre>' + 'Email     : ' + user.email + '</pre>' + '</h3>' +
              '<h3>' + '<pre>' + 'Gender    : ' + user.gender + '</pre>' + '</h3>' +
            '</div>' +
          '</div>' +
          '<div>' +
            '<input id="postInput" type="text" class="post" placeholder="Apa yang Anda pikirkan?" />' +
            '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
            '<button class="logout" onclick="facebookLogout()">Logout</button>'
       )
     })

     getUserFeed(feed => {
       feed.data.map(value => {
         html = ''
         if (value.message || value.story){
           html+='<div class="list-group-item clearfix">';
           if (value.message) {
             html+='<div>' + value.message + '</div>';
           }
           if (value.story) {
             html += '<div>' + value.story + '</div>';
           }
           html += '<button type="button" class="pull-right btn btn-xs btn-danger"' +
            'onClick="deletePost(\'' + value.id + '\')">' +
            '&times; Remove Post' +
            '</button>' +
            '</div>';
          }
          $('#lab8').append(html)
          });
        });
      }
   else {
     $('#lab8').html('<button type="button" class="btn btn-primary" onclick="facebookLogin()">Login to Facebook</button>');
   }
 };

 const facebookLogin = () => {
   FB.login(function(response){
     console.log(response);
   }, {scope:'public_profile, user_posts, publish_actions'})
 };

 const facebookLogout = () => {
   FB.getLoginStatus(function(response){
     if (response.status === 'connected') {
       FB.logout();
     }
     render(false);
   })
 };

 const getUserData = (fun) => {
   FB.api(
     "/me?fields=name, cover, picture, about, email, gender",
     function(response){
       console.log(response)
       fun(response);
     });
  };

 const getUserFeed = (fun) => {
   FB.api(
     "/me/feed",
     function(response){
       console.log(response)
       fun(response)
     }
   )
 }

 const postFeed = (message) => {
   FB.api(
     "/me/feed",
     "POST",
     {
       "message" : message
     },
   )
 };

 const postStatus = () => {
   const message = $('#postInput').val();
   postFeed(message);
 }

 const deletePost = (postId) => {
   FB.api(postId, 'delete', function(response){
     if (response && !response.error) {
       render(true)
     }
     else {
       alert('Error!')
     }
   })
 }
